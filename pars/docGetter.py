__author__ = 'szymon & mateusz'

import urllib2

glossaryElems = {'action': 'http://docs.ansible.com/glossary.html#action',
                 'async': 'http://docs.ansible.com/glossary.html#async',
                 'connection': 'http://docs.ansible.com/glossary.html#connection-type-connection-plugin',
                 'notify': 'http://docs.ansible.com/glossary.html#notify',
                 'when': 'http://docs.ansible.com/glossary.html#when',
                 'local_action': 'http://docs.ansible.com/glossary.html#local-action',
                 'tasks': 'http://docs.ansible.com/glossary.html#tasks',
                 'hosts': 'http://docs.ansible.com/glossary.html#host-specifier',
                 'with_file': 'http://docs.ansible.com/glossary.html#lookup-plugin',
                 'with_items': 'http://docs.ansible.com/glossary.html#lookup-plugin',
                 'serial': 'http://docs.ansible.com/glossary.html#rolling-update',
                 'vars': 'http://docs.ansible.com/glossary.html#vars-variables'}

modulePageLinkPattern = 'http://docs.ansible.com/{}_module.html'

def checkLink(addr):
    req = urllib2.Request(addr)
    try:
        urllib2.urlopen(req)
    except urllib2.URLError, e:
        if e.code == 404:
            return False
    else:
        return True

def getLink(module):
    if module in glossaryElems:
        return glossaryElems[module]
    else:
        return modulePageLinkPattern.format(module.replace('_', '-'))


def getLinksInDict(d):
    for k in d.keys():
        yield getLink(k)

def getLinksForList(l):
    result = []
    for elem in l:
        if isinstance(elem, dict):
            result.extend(getLinksInDict(elem))
        if isinstance(elem, list):
            result.extend(getLinksForList(elem))
    return result


def getLinksForFile(roleFile):
    return getLinksForList(roleFile.content)


# class Tmp(object):
#     content = []
#
# if __name__ == '__main__':
#     o = Tmp()
#     o.content = [{'name': 'bla'}, [{'module': 'oasifj'}]]
#
#     print(getLinksForFile(o))