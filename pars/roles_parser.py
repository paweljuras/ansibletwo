import os
from role import Role
from pars.config import DIR_TO_ROLES


def get_role_list(dir):
    roles = []
    roles_list = os.listdir(dir)
    for role in roles_list:
        if os.path.isdir(dir + os.sep + role):
            roles.append(Role(role))

    return roles


def parse():
    roles = get_role_list(DIR_TO_ROLES)
    for role in roles:
        role.parse_role(DIR_TO_ROLES + os.sep + role.name)

    return roles 

def save_doc_for_role(doc, path_to_role):
    f = open(path_to_role + os.sep + 'doc', 'w')
    f.write(doc)
    f.close()

def get_doc_for_role(path_to_role):
    return "".join(open(path_to_role + os.sep + 'doc','r').readlines())
    


if __name__=='__main__':

    # save_doc_for_role("wacek123467\nWACEK2", "/home/p/TWO/ansibletwo/ansible-examples-master/hadoop/roles/common" )
    # print get_doc_for_role("/home/p/TWO/ansibletwo/ansible-examples-master/hadoop/roles/common" )

    roles = parse()
    for r in roles:
        print r.to_string()