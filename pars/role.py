import os
import yaml
import json

pathdict = {}


class Dir:
    def is_dir(self):
        return True

    def __init__(self, dir_to_dir):
        self.files = []
        self.location = dir_to_dir
        self.file_list = os.listdir(dir_to_dir)
        for file_name in self.file_list:
            if os.path.isfile(dir_to_dir + os.sep + file_name):
                if os.path.isfile(dir_to_dir + os.sep + file_name) and not file_name.startswith('.'):
                    f = File(dir_to_dir + os.sep + file_name)
                    self.files.append(f)
                    pathdict[f.location] = f

            else:
                self.files.append(Dir(dir_to_dir + os.sep + file_name))



    def to_string(self, s):
        ret = s + 'Dir: ' + self.location #+ ' ' + str(len(self.files))

        for f in self.files:
            ret = ret + '\n' + f.to_string(s + '\t')

        return ret

class File:
    isyaml = False
    location = ''
    content = None

    def is_dir(self):
        return False

    def __init__(self,dir_to_file):
        self.location = dir_to_file
        name = dir_to_file.split(os.sep)[-1]
        if name.strip().endswith('yml'):
            self.isyaml = True
            f = open(dir_to_file)
            self.content = yaml.load(f)
        else:
            self.isyaml = False
            with open(dir_to_file, 'r') as src:
                self.content = src.read()


    def set_content(self, content):
        if self.isyaml and isinstance(content, basestring):
            content = yaml.load(content)
        self.content = content


    def update_file(self):
        with open(self.location, 'w') as stream:
            if self.isyaml:
                yaml.dump(self.content, stream)
            else:
                stream.write(self.content)

    def pretty(self):
        if self.isyaml:
            return yaml.dump(self.content)
        else:
            return self.content

        
    def to_string(self, s):
        ret =  s + 'File: ' + self.location
        #ret = ret + '\n' + s + 'isyaml: ' + str(self.isyaml)
        return ret


class Role:
    def __init__(self,name):
        self.name = name


    def parse_role(self,dir_to_role): 
        self.propeties = []
        if os.path.isdir(dir_to_role) and not dir_to_role.split(os.sep)[-1].startswith('.'):
            directories = os.listdir(dir_to_role)
            for directory in directories:
                if os.path.isdir(dir_to_role + os.sep + directory):
                    self.propeties.append(Dir(dir_to_role + os.sep + directory))

    def to_string(self):
        ret = 'Role: ' + self.name
        for prop in self.propeties:
            ret = ret + '\n' + prop.to_string('\t')

        return ret



