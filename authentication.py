import ldap #http://www.python-ldap.org/
import ldap.sasl
import sys


def testUser(username, password, url):


    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
    l = ldap.initialize(url)
    l.protocol_version = ldap.VERSION3

    dn = r"uid=" + username + r",ou=Administrators,ou=People,dc=iisg,dc=agh,dc=edu,dc=pl"

    try:
        a = l.simple_bind_s(dn, password)
        l.unbind_s()
        return True
    except Exception as e:
        l.unbind_s()
        return False

def login(username, password):

    url = r"ldaps://astrid.iisg.agh.edu.pl:636"
    dn = r"cn=labauth,dc=iisg,dc=agh,dc=edu,dc=pl"
    dnPass = r"eexoosaaMo1Eem0Ahth3"
    adminGroup = r"ou=Administrators,ou=People,dc=iisg,dc=agh,dc=edu,dc=pl"

    try:
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        l = ldap.initialize(url)
        l.protocol_version = ldap.VERSION3
        l.simple_bind_s(dn, dnPass)
        entries = l.search_s(adminGroup, ldap.SCOPE_ONELEVEL)
        l.unbind_s()

        for entry in entries:
            if username == entry[1]['uid'][0]:
                #return testUser(username, password, url) tu odkomentowac testowanie hasla !!!+!!!!!+!+!+!+!+!+!+!+!
                return True
        return False


    except ldap.INVALID_CREDENTIALS:
        print('wrong creditentials provided')
        l.unbind_s()
        return False
    except Exception as e:
        print "Unexpected error:", sys.exc_info()[0]
        print e.__str__()

#print login("faberadm", "asdasdasd")