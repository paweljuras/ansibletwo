import web
import re
import base64
import os, sys, inspect
import authentication
import json


authenticationEnabled = False

from lock import Locker

#dodanie do sciezki pythona bo na windowsie nie dzialalo
def addSubfolderToPath(folderName): #strasznie to nieladne ale chcieliscie w innym folderze to macie :P
    cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
    if cmd_folder not in sys.path:
        sys.path.insert(0, cmd_folder)
    cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],folderName)))
    if cmd_subfolder not in sys.path:
        sys.path.insert(0, cmd_subfolder)

addSubfolderToPath("pars")
from roles_parser import parse
from docGetter import getLinksForFile
import role

render = web.template.render('templates/')

urls = (
    '/', 'index',
    '/login','login',
    '/lock', 'lock',
    '/unlock', 'unlock',
    '/update', 'update'
)

app = web.application(urls, globals())

allowed = (
    ('jon','pass11'),
    ('tom','pass2')
)

class index:
    def GET(self):
        global authenticationEnabled
        if web.ctx.env.get('HTTP_AUTHORIZATION') is not None or not authenticationEnabled:
            roles = parse()    
            return render.index(roles)
        else:
            raise web.seeother('/login')

class login:
    def GET(self):
        auth = web.ctx.env.get('HTTP_AUTHORIZATION')
        authreq = False
        if auth is None:
            authreq = True
        else:
            auth = re.sub('^Basic ','',auth)
            username,password = base64.decodestring(auth).split(':')
            isAuthorized = authentication.login(username, password)
            if isAuthorized:
                raise web.seeother('/')
            else:
                authreq = True
        if authreq:
            web.header('WWW-Authenticate','Basic realm="Auth example"')
            web.ctx.status = '401 Unauthorized'
            return
        roles = parse()   
        return render.index(roles)

class lock:
    def GET(self):
        args = web.input()
        if Locker.lock_role(args['role']):
            print "LOCKING: " + args['role']
            links = getLinksForFile(role.pathdict[args['role']])
            # print "LINKS: " + links
            return role.pathdict[args['role']].pretty() + "SUPERHIDDENSEPARATOR" + str(links)
            # TODO ZWROCIC WSZYSTKIE DANE NA TEMAT ROLI
        else:
            return "ROLE IS CURRENTLY BEING EDITED BY SOMEONE ELSE"

class update:
    def GET(self):
        args = web.input()
        r = role.pathdict[args['location']]
        jsondict = json.loads(args['content'])
        con = jsondict["content"]
        r.set_content(con)
        r.update_file()

class unlock:
    def GET(self):
        args = web.input()
        print "UNLOCKING: " + args['role']

        if Locker.release_role(args['role']):
            return "unlocked"
    
        return 'was not locked'

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()

