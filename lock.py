import time
from threading import Thread, Lock

mutex = Lock()

currently_lock_roles = []

class Locker():

    @classmethod
    def lock_role(self,role):
        mutex.acquire()
        if role not in currently_lock_roles:
            currently_lock_roles.append(role)
            t = Thread(target = start_editing_timeout, args=(role,))
            t.start()
            mutex.release()
            return True
        else:
            mutex.release()
            return False


    @classmethod
    def release_role(self,role):
        mutex.acquire()
        if role in currently_lock_roles:
            currently_lock_roles.remove(role)
            mutex.release()
            return True
        else:
            mutex.release()
            return False

def start_editing_timeout(role):
    # time.sleep(1000*60*15)
    time.sleep(1*60*15)
    mutex.acquire()
    if role in currently_lock_roles:
        currently_lock_roles.remove(role)
        print("RELEASING: " + role)
        mutex.release()